import emails
import os
import datetime
import logging
import time
from asyncio import *
from jinja2 import Template
from openpyxl import load_workbook
from multiprocessing import Queue

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s', datefmt='%d/%m/%Y %H:%M:%S', level=logging.DEBUG, handlers = [
            logging.FileHandler("sellers.txt"),
            logging.StreamHandler()])

class BaseDatos:
    def __init__(self):
        self.empresas = []

db = BaseDatos()
db.empresas = []
Valores = []

class Empresa:
    def __init__(self):
        self.id = 0
        self.nombre = "Null"
        self.valores = dict()


def process_first_row(cell, empresa):
    if 14 <= cell.column <= 21:
        Valores.append(cell.value)
        return

def process_cell(cell, empresa):
    if cell.column == 1:
        empresa.id = cell.value
        return
    if cell.column == 2:
        empresa.short_code = cell.value
        return
    if cell.column == 3:
        empresa.supplier = cell.value
        empresa.supplier = empresa.supplier.replace('/', '')
        return
    if cell.column == 13:
        empresa.score = cell.value
        return
    if cell.column == 14:
        empresa.nmv_items = cell.value
        empresa.valores[Valores[0]] = cell.value
        return
    if cell.column == 15:
        empresa.on_time = cell.value
        empresa.valores[Valores[1]] = cell.value
        return
    if cell.column == 16:
        empresa.cancellations = cell.value
        empresa.valores[Valores[2]] = cell.value
        return
    if cell.column == 17:
        empresa.returns = cell.value
        empresa.valores[Valores[3]] = cell.value
        return
    if cell.column == 18:
        empresa.fbl = cell.value
        empresa.valores[Valores[4]] = cell.value
        return
    if cell.column == 19:
        empresa.skus_purchasables = cell.value
        empresa.valores[Valores[5]] = cell.value
        return
    if cell.column == 20:
        empresa.antiguedad = cell.value
        empresa.valores[Valores[6]] = cell.value
        return
    if cell.column == 21:
        empresa.content_score = cell.value
        empresa.valores[Valores[7]] = cell.value


def create_db():
    wb = load_workbook(filename = 'score.xlsx', read_only=True)
    sheet_ranges = wb['Sellers']

    for idx, row in enumerate(sheet_ranges.rows):
        empresa = Empresa()
        for cell in row:
            if cell.value == None:
                break
            if (idx == 0):
                process_first_row(cell, empresa)
            else:
                process_cell(cell, empresa)
        db.empresas.append(empresa)
    logging.info("Leidas {0} empresas".format(len(db.empresas)))
            
            #print("{0} \t".format(cell.value), end='')
        #print()
    return db

now = datetime.datetime.now()
base_datos = create_db()

logging.info("Creando templates...")
start_time = time.time()

try:
    if not os.path.isdir("output"):
        os.makedirs("output")
    i=0
    for empresa in base_datos.empresas:
        i+=1
        if not empresa.id == 0:
            html = open('Template.html', 'r', encoding='utf-8')
            template = Template(html.read())
            template_output = template.render(score=empresa.score, empresa=empresa.supplier, dict_item=empresa.valores)
            html.close()
            text_file = open("output" + os.sep + empresa.supplier + "_" + now.strftime("%d-%m-%Y") + "_" + str(empresa.id) + ".html", "w")
            text_file.write(template_output)
            text_file.close()
    execution_time = time.time() - start_time
    logging.info("Se crearon {0} templates en {1} segundos".format(i, execution_time))
except:
    logging.error("Hubo un error creando los templates")
    raise
