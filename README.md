SELLERS
=======

Instalacion
-----------

El software se divide en dos programas que pueden encontrarse en dos zips. Deben ser descomprimidos en la misma carpeta.

* generador_de_templates.zip
* mails.zip

generador_de_templates: como bien su nombre indica, genera templates a partir de un archivo llamado "score.xlsx".
Dentro de este archivo excel debe existir una hoja llamada "Sellers" de donde el software se encarga de extraer la informacion necesaria.
Este programa esta compuesto de los siguientes archivos:

* generador_de_templates.py: el ejecutable, se precisa tener python 3.6 instalado.
* Template.html: el template base que especifica como deben crearse los templates seran creados por generador_de_templates.py
* output/: es una carpeta. El output de generador_de_templates

mails: envia los contenidos de la carpeta "output/" a cada destinatario segun la configuracion, especificada en la hoja "Mails" en "score.xlsx".
Debe configurarse el mail desde el que se van a enviar los archivos (referirse a la seccion Configuracion)
Este programa esta compuesto de los siguientes archivos:

* mails.exe: el ejecutable
* config.ini: la configuracion de la cuenta que va a enviar los mails, como tambien el asunto, entre otras cosas (mas informacion en Configuracion)


Configuracion
-------------

A continuacion hay un ejemplo comentado de config.ini

~~~~
[GENERAL]

# El modo le indica al programa que modulo cargar.
# Puede aceptar tres valores: finanzas, bna, sellers.
# En este caso sellers va por default ya que nos encontramos en el modulo sellers
modo=sellers

# "debug" le indica al programa si los mails que van a ser enviados son pruebas o no.
# Cuando se encuentre en 1, el programa cambia el destinatario de los mails a pruebasmailsbna@gmail.com en vez de enviarlo a los sellers.
# Sirve para hacer envíos de prueba.
# Cuando se encuentre en 0, el programa va a enviar los mails a los sellers.
debug=1

[SELLERS]
usuario=usuario@linio.com
contrasena=password
# El asunto de los mails a enviar
asunto=Facturas Linio

[SMTP]
# El usuario linio.com usa los servidores de office365. En caso de usar una casilla gmail.com se debe usar smtp.gmail.com
smtp=smtp.office365.com
# El puerto 587 es el default y solo deberia ser cambiado a 465 si se quiere enviar mails usando una casilla de gmail.
puerto=587

~~~~
