from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [], excludes = [], includes = ["idna.idnadata", 'asyncio', 'asyncio.compat', 'asyncio.constants', 'asyncio.base_futures', 'asyncio.base_tasks', 'asyncio.base_subprocess', 'asyncio.proactor_events'])

base = 'Console'

executables = [
    Executable('template.py', base=base, targetName = 'template.exe')
]

setup(name='template',
      version = '1.0',
      description = '',
      options = dict(build_exe = buildOptions),
      executables = executables)
